## Привет, коллега 👋!

Это мой основной профиль по **iOS**-разработке. Ссылки на проекты, написанные на языке **Swift**, находятся ниже. Также развиваюсь в направлении веб-разработки и страничку можно посмотреть на [GitHub](https://github.com/Johnsterr)

#### Мои проекты:

- [Business Card - визитка](https://gitlab.com/Sterr/business-card)
- [Flashlight - фонарик](https://gitlab.com/Sterr/flashlight)
- [Apple Pie - игра-виселица](https://gitlab.com/Sterr/apple-pie)
- [Quiz - викторина](https://gitlab.com/Sterr/quiz)
- [AR Basketball - Баскетбол в дополненной реальности](https://gitlab.com/Sterr/ar-basketball)
- [AR Reality Glasses - Маски в дополненной реальности](https://gitlab.com/Sterr/ar-reality-glasses)
- [AR Drawing - Рисование в дополненной реальности](https://gitlab.com/Sterr/ar-drawing)
- [Emoji Dictionary - Словарь эмоджи](https://gitlab.com/Sterr/table-views-emoji)
- [Hotel Manzana - Отель Манзана](https://gitlab.com/Sterr/hotel-manzana)
- [ToDo List - Список дел](https://gitlab.com/Sterr/todo-list)
- [Restaurant - Приложение Ресторан](https://gitlab.com/Sterr/restaurant)
- [Enstagram - клон Instagram](https://gitlab.com/Sterr/Enstagram) (в процессе разработки...)

